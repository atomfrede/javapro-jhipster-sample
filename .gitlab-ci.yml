image: jhipster/jhipster:v6.1.0

cache:
    key: "$CI_COMMIT_REF_NAME"
    paths:
        - .gradle/
stages:
    - build
    - test
    - analyze
    - package
    - release
    - post-release
    - deploy
    - dast

before_script:
    - export NG_CLI_ANALYTICS="false"
    - export GRADLE_USER_HOME=`pwd`/.gradle

include:
  - template: Code-Quality.gitlab-ci.yml
  - template: SAST.gitlab-ci.yml
  - template: DAST.gitlab-ci.yml
  - template: Container-Scanning.gitlab-ci.yml
  - local: 'container-scanning.yml'
  
gradle-compile:
    stage: build
    before_script:
        - export NG_CLI_ANALYTICS="false"
        - export GRADLE_USER_HOME=`pwd`/.gradle
        - ./gradlew npm_install -PnodeInstall --no-daemon
    script:
        - ./gradlew compileJava -x check -PnodeInstall --no-daemon
    artifacts:
        paths:
          - build/classes/
          - build/generated/
        expire_in: 1 day

gradle-test:
    stage: test
    before_script:
        - export NG_CLI_ANALYTICS="false"
        - export GRADLE_USER_HOME=`pwd`/.gradle
        - ./gradlew npm_install -PnodeInstall --no-daemon
    script:
        - ./gradlew test -PnodeInstall --no-daemon
    artifacts:
        reports:
            junit: build/test-results/test/TEST-*.xml
        paths:
            - build/test-results/
            - build/jacoco/
        expire_in: 1 day

gradle-integration-test:
    stage: test
    before_script:
        - export NG_CLI_ANALYTICS="false"
        - export GRADLE_USER_HOME=`pwd`/.gradle
        - ./gradlew npm_install -PnodeInstall --no-daemon
    script:
        - ./gradlew integrationTest -PnodeInstall --no-daemon
    artifacts:
        reports:
            junit: build/test-results/integrationTest/TEST-*.xml
        paths:
            - build/test-results/
            - build/jacoco/
        expire_in: 1 day

frontend-test:
    stage: test
    before_script:
        - export NG_CLI_ANALYTICS="false"
        - export GRADLE_USER_HOME=`pwd`/.gradle
        - ./gradlew npm_install -PnodeInstall --no-daemon
    script:
        - ./gradlew npm_run_test -PnodeInstall --no-daemon
    artifacts:
        reports:
            junit: build/test-results/TESTS-results-jest.xml
        paths:
            - build/test-results/
            - build/jacoco/
        expire_in: 1 day

sonar-analyze:
    stage: analyze
    before_script:
        - export NG_CLI_ANALYTICS="false"
        - export GRADLE_USER_HOME=`pwd`/.gradle
        - ./gradlew npm_install -PnodeInstall --no-daemon
    dependencies:
        - gradle-test
        - gradle-integration-test
        - frontend-test
    script:
        - ./gradlew sonarqube --no-daemon -Dsonar.organization=atomfrede-github -Dsonar.host.url=https://sonarcloud.io -Dsonar.login=$SONAR_TOKEN
    allow_failure: true

gradle-package:
    stage: package
    before_script:
        - export NG_CLI_ANALYTICS="false"
        - export GRADLE_USER_HOME=`pwd`/.gradle
        - ./gradlew npm_install -PnodeInstall --no-daemon
    script:
        - ./gradlew bootJar -Pprod -x check --no-daemon
    artifacts:
        paths:
            - build/libs/*.jar
            - build/classes
        expire_in: 1 day

# Uncomment the following line to use gitlabs container registry. You need to adapt the REGISTRY_URL in case you are not using gitlab.com
docker-push:
   stage: release
   before_script:
       - export NG_CLI_ANALYTICS="false"
       - export GRADLE_USER_HOME=`pwd`/.gradle
       - ./gradlew npm_install -PnodeInstall --no-daemon
   variables:
       REGISTRY_URL: registry.gitlab.com
       IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
   dependencies:
       - gradle-package
   script:
       - ./gradlew jib -Djib.to.image="$IMAGE_TAG" -Djib.to.auth.username="gitlab-ci-token"  -Djib.to.auth.password="$CI_BUILD_TOKEN"

deploy-to-production:
    stage: deploy
    before_script:
        - export NG_CLI_ANALYTICS="false"
        - export GRADLE_USER_HOME=`pwd`/.gradle
        - ./gradlew npm_install -PnodeInstall --no-daemon
    script:
        - ./gradlew deployHeroku --no-daemon
    environment:
        name: production
        url: https://javaprosample.herokuapp.com/
    when: manual
    
dast:
  stage: dast # IMPORTANT: don't forget to add this
  variables:
    DAST_WEBSITE: https://javaprosample.herokuapp.com/

