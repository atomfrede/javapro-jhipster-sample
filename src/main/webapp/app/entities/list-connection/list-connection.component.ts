import { mixins } from 'vue-class-component';
import { Component, Inject, Vue } from 'vue-property-decorator';
import { IListConnection } from '@/shared/model/list-connection.model';
import AlertService from '@/shared/alert/alert.service';

import ListConnectionService from './list-connection.service';

@Component
export default class ListConnection extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('listConnectionService') private listConnectionService: () => ListConnectionService;
  private removeId: number = null;
  public listConnections: IListConnection[] = [];

  public dismissCountDown: number = this.$store.getters.dismissCountDown;
  public dismissSecs: number = this.$store.getters.dismissSecs;
  public alertType: string = this.$store.getters.alertType;
  public alertMessage: any = this.$store.getters.alertMessage;

  public getAlertFromStore() {
    this.dismissCountDown = this.$store.getters.dismissCountDown;
    this.dismissSecs = this.$store.getters.dismissSecs;
    this.alertType = this.$store.getters.alertType;
    this.alertMessage = this.$store.getters.alertMessage;
  }

  public countDownChanged(dismissCountDown: number) {
    this.alertService().countDownChanged(dismissCountDown);
    this.getAlertFromStore();
  }

  public mounted(): void {
    this.retrieveAllListConnections();
  }

  public clear(): void {
    this.retrieveAllListConnections();
  }

  public retrieveAllListConnections(): void {
    this.listConnectionService()
      .retrieve()
      .then(res => {
        this.listConnections = res.data;
      });
  }

  public prepareRemove(instance: IListConnection): void {
    this.removeId = instance.id;
  }

  public removeListConnection(): void {
    this.listConnectionService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('javaprosampleApp.listConnection.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllListConnections();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
