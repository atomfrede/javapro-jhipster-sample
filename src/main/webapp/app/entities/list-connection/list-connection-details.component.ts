import { Component, Vue, Inject } from 'vue-property-decorator';

import { IListConnection } from '@/shared/model/list-connection.model';
import ListConnectionService from './list-connection.service';

@Component
export default class ListConnectionDetails extends Vue {
  @Inject('listConnectionService') private listConnectionService: () => ListConnectionService;
  public listConnection: IListConnection = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.listConnectionId) {
        vm.retrieveListConnection(to.params.listConnectionId);
      }
    });
  }

  public retrieveListConnection(listConnectionId) {
    this.listConnectionService()
      .find(listConnectionId)
      .then(res => {
        this.listConnection = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
