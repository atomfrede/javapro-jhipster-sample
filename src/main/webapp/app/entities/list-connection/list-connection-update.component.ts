import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import IssueListService from '../issue-list/issue-list.service';
import { IIssueList } from '@/shared/model/issue-list.model';

import WorkspaceService from '../workspace/workspace.service';
import { IWorkspace } from '@/shared/model/workspace.model';

import AlertService from '@/shared/alert/alert.service';
import { IListConnection, ListConnection } from '@/shared/model/list-connection.model';
import ListConnectionService from './list-connection.service';

const validations: any = {
  listConnection: {
    name: {
      required
    },
    type: {
      required
    }
  }
};

@Component({
  validations
})
export default class ListConnectionUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('listConnectionService') private listConnectionService: () => ListConnectionService;
  public listConnection: IListConnection = new ListConnection();

  @Inject('issueListService') private issueListService: () => IssueListService;

  public issueLists: IIssueList[] = [];

  @Inject('workspaceService') private workspaceService: () => WorkspaceService;

  public workspaces: IWorkspace[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.listConnectionId) {
        vm.retrieveListConnection(to.params.listConnectionId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.listConnection.id) {
      this.listConnectionService()
        .update(this.listConnection)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('javaprosampleApp.listConnection.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.listConnectionService()
        .create(this.listConnection)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('javaprosampleApp.listConnection.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveListConnection(listConnectionId): void {
    this.listConnectionService()
      .find(listConnectionId)
      .then(res => {
        this.listConnection = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.issueListService()
      .retrieve()
      .then(res => {
        this.issueLists = res.data;
      });
    this.workspaceService()
      .retrieve()
      .then(res => {
        this.workspaces = res.data;
      });
  }
}
