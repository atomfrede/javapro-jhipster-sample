import { Component, Vue, Inject } from 'vue-property-decorator';

import { IWorkspace } from '@/shared/model/workspace.model';
import WorkspaceService from './workspace.service';

@Component
export default class WorkspaceDetails extends Vue {
  @Inject('workspaceService') private workspaceService: () => WorkspaceService;
  public workspace: IWorkspace = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.workspaceId) {
        vm.retrieveWorkspace(to.params.workspaceId);
      }
    });
  }

  public retrieveWorkspace(workspaceId) {
    this.workspaceService()
      .find(workspaceId)
      .then(res => {
        this.workspace = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
