import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import TeamService from '../team/team.service';
import { ITeam } from '@/shared/model/team.model';

import ListConnectionService from '../list-connection/list-connection.service';
import { IListConnection } from '@/shared/model/list-connection.model';

import AlertService from '@/shared/alert/alert.service';
import { IWorkspace, Workspace } from '@/shared/model/workspace.model';
import WorkspaceService from './workspace.service';

const validations: any = {
  workspace: {
    name: {
      required
    }
  }
};

@Component({
  validations
})
export default class WorkspaceUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('workspaceService') private workspaceService: () => WorkspaceService;
  public workspace: IWorkspace = new Workspace();

  @Inject('teamService') private teamService: () => TeamService;

  public teams: ITeam[] = [];

  @Inject('listConnectionService') private listConnectionService: () => ListConnectionService;

  public listConnections: IListConnection[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.workspaceId) {
        vm.retrieveWorkspace(to.params.workspaceId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.workspace.id) {
      this.workspaceService()
        .update(this.workspace)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('javaprosampleApp.workspace.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.workspaceService()
        .create(this.workspace)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('javaprosampleApp.workspace.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveWorkspace(workspaceId): void {
    this.workspaceService()
      .find(workspaceId)
      .then(res => {
        this.workspace = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.teamService()
      .retrieve()
      .then(res => {
        this.teams = res.data;
      });
    this.listConnectionService()
      .retrieve()
      .then(res => {
        this.listConnections = res.data;
      });
  }
}
