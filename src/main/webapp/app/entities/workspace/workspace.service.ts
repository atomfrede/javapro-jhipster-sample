import axios from 'axios';

import buildPaginationQueryOpts from '@/shared/sort/sorts';

import { IWorkspace } from '@/shared/model/workspace.model';

const baseApiUrl = 'api/workspaces';

export default class WorkspaceService {
  public find(id: number): Promise<IWorkspace> {
    return new Promise<IWorkspace>(resolve => {
      axios.get(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public retrieve(paginationQuery?: any): Promise<any> {
    return new Promise<any>(resolve => {
      axios.get(baseApiUrl + `?${buildPaginationQueryOpts(paginationQuery)}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public delete(id: number): Promise<any> {
    return new Promise<any>(resolve => {
      axios.delete(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public create(entity: IWorkspace): Promise<IWorkspace> {
    return new Promise<IWorkspace>(resolve => {
      axios.post(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public update(entity: IWorkspace): Promise<IWorkspace> {
    return new Promise<IWorkspace>(resolve => {
      axios.put(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }
}
