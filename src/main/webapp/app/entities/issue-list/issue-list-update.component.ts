import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import IssueService from '../issue/issue.service';
import { IIssue } from '@/shared/model/issue.model';

import ListConnectionService from '../list-connection/list-connection.service';
import { IListConnection } from '@/shared/model/list-connection.model';

import AlertService from '@/shared/alert/alert.service';
import { IIssueList, IssueList } from '@/shared/model/issue-list.model';
import IssueListService from './issue-list.service';

const validations: any = {
  issueList: {
    name: {
      required
    }
  }
};

@Component({
  validations
})
export default class IssueListUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('issueListService') private issueListService: () => IssueListService;
  public issueList: IIssueList = new IssueList();

  @Inject('issueService') private issueService: () => IssueService;

  public issues: IIssue[] = [];

  @Inject('listConnectionService') private listConnectionService: () => ListConnectionService;

  public listConnections: IListConnection[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.issueListId) {
        vm.retrieveIssueList(to.params.issueListId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.issueList.id) {
      this.issueListService()
        .update(this.issueList)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('javaprosampleApp.issueList.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.issueListService()
        .create(this.issueList)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('javaprosampleApp.issueList.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveIssueList(issueListId): void {
    this.issueListService()
      .find(issueListId)
      .then(res => {
        this.issueList = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.issueService()
      .retrieve()
      .then(res => {
        this.issues = res.data;
      });
    this.listConnectionService()
      .retrieve()
      .then(res => {
        this.listConnections = res.data;
      });
  }
}
