import { Component, Vue, Inject } from 'vue-property-decorator';

import { IIssueList } from '@/shared/model/issue-list.model';
import IssueListService from './issue-list.service';

@Component
export default class IssueListDetails extends Vue {
  @Inject('issueListService') private issueListService: () => IssueListService;
  public issueList: IIssueList = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.issueListId) {
        vm.retrieveIssueList(to.params.issueListId);
      }
    });
  }

  public retrieveIssueList(issueListId) {
    this.issueListService()
      .find(issueListId)
      .then(res => {
        this.issueList = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
