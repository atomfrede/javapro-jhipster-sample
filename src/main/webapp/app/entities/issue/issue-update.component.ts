import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import IssueListService from '../issue-list/issue-list.service';
import { IIssueList } from '@/shared/model/issue-list.model';

import AlertService from '@/shared/alert/alert.service';
import { IIssue, Issue } from '@/shared/model/issue.model';
import IssueService from './issue.service';

const validations: any = {
  issue: {
    title: {
      required
    }
  }
};

@Component({
  validations
})
export default class IssueUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('issueService') private issueService: () => IssueService;
  public issue: IIssue = new Issue();

  @Inject('issueListService') private issueListService: () => IssueListService;

  public issueLists: IIssueList[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.issueId) {
        vm.retrieveIssue(to.params.issueId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.issue.id) {
      this.issueService()
        .update(this.issue)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('javaprosampleApp.issue.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.issueService()
        .create(this.issue)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('javaprosampleApp.issue.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveIssue(issueId): void {
    this.issueService()
      .find(issueId)
      .then(res => {
        this.issue = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.issueListService()
      .retrieve()
      .then(res => {
        this.issueLists = res.data;
      });
  }
}
