import Vue from 'vue';
import Component from 'vue-class-component';
Component.registerHooks([
  'beforeRouteEnter',
  'beforeRouteLeave',
  'beforeRouteUpdate' // for vue-router 2.2+
]);
import Router from 'vue-router';
const Home = () => import('../core/home/home.vue');
const Error = () => import('../core/error/error.vue');
const Register = () => import('../account/register/register.vue');
const Activate = () => import('../account/activate/activate.vue');
const ResetPasswordInit = () => import('../account/reset-password/init/reset-password-init.vue');
const ResetPasswordFinish = () => import('../account/reset-password/finish/reset-password-finish.vue');
const ChangePassword = () => import('../account/change-password/change-password.vue');
const Settings = () => import('../account/settings/settings.vue');
const JhiUserManagementComponent = () => import('../admin/user-management/user-management.vue');
const JhiUserManagementViewComponent = () => import('../admin/user-management/user-management-view.vue');
const JhiUserManagementEditComponent = () => import('../admin/user-management/user-management-edit.vue');
const JhiConfigurationComponent = () => import('../admin/configuration/configuration.vue');
const JhiDocsComponent = () => import('../admin/docs/docs.vue');
const JhiHealthComponent = () => import('../admin/health/health.vue');
const JhiLogsComponent = () => import('../admin/logs/logs.vue');
const JhiAuditsComponent = () => import('../admin/audits/audits.vue');
const JhiMetricsComponent = () => import('../admin/metrics/metrics.vue');
/* tslint:disable */
// prettier-ignore
const Team = () => import('../entities/team/team.vue');
// prettier-ignore
const TeamUpdate = () => import('../entities/team/team-update.vue');
// prettier-ignore
const TeamDetails = () => import('../entities/team/team-details.vue');
// prettier-ignore
const Workspace = () => import('../entities/workspace/workspace.vue');
// prettier-ignore
const WorkspaceUpdate = () => import('../entities/workspace/workspace-update.vue');
// prettier-ignore
const WorkspaceDetails = () => import('../entities/workspace/workspace-details.vue');
// prettier-ignore
const IssueList = () => import('../entities/issue-list/issue-list.vue');
// prettier-ignore
const IssueListUpdate = () => import('../entities/issue-list/issue-list-update.vue');
// prettier-ignore
const IssueListDetails = () => import('../entities/issue-list/issue-list-details.vue');
// prettier-ignore
const Issue = () => import('../entities/issue/issue.vue');
// prettier-ignore
const IssueUpdate = () => import('../entities/issue/issue-update.vue');
// prettier-ignore
const IssueDetails = () => import('../entities/issue/issue-details.vue');
// prettier-ignore
const ListConnection = () => import('../entities/list-connection/list-connection.vue');
// prettier-ignore
const ListConnectionUpdate = () => import('../entities/list-connection/list-connection-update.vue');
// prettier-ignore
const ListConnectionDetails = () => import('../entities/list-connection/list-connection-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

Vue.use(Router);

// prettier-ignore
export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/forbidden',
      name: 'Forbidden',
      component: Error,
      meta: { error403: true }
    },
    {
      path: '/not-found',
      name: 'NotFound',
      component: Error,
      meta: { error404: true }
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/activate',
      name: 'Activate',
      component: Activate
    },
    {
      path: '/reset/request',
      name: 'ResetPasswordInit',
      component: ResetPasswordInit
    },
    {
      path: '/reset/finish',
      name: 'ResetPasswordFinish',
      component: ResetPasswordFinish
    },
    {
      path: '/account/password',
      name: 'ChangePassword',
      component: ChangePassword,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/account/settings',
      name: 'Settings',
      component: Settings,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/admin/user-management',
      name: 'JhiUser',
      component: JhiUserManagementComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/user-management/new',
      name: 'JhiUserCreate',
      component: JhiUserManagementEditComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/user-management/:userId/edit',
      name: 'JhiUserEdit',
      component: JhiUserManagementEditComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/user-management/:userId/view',
      name: 'JhiUserView',
      component: JhiUserManagementViewComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/docs',
      name: 'JhiDocsComponent',
      component: JhiDocsComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/audits',
      name: 'JhiAuditsComponent',
      component: JhiAuditsComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/jhi-health',
      name: 'JhiHealthComponent',
      component: JhiHealthComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/logs',
      name: 'JhiLogsComponent',
      component: JhiLogsComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/jhi-metrics',
      name: 'JhiMetricsComponent',
      component: JhiMetricsComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/jhi-configuration',
      name: 'JhiConfigurationComponent',
      component: JhiConfigurationComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    }
    ,
    {
      path: '/entity/team',
      name: 'Team',
      component: Team,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/team/new',
      name: 'TeamCreate',
      component: TeamUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/team/:teamId/edit',
      name: 'TeamEdit',
      component: TeamUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/team/:teamId/view',
      name: 'TeamView',
      component: TeamDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/workspace',
      name: 'Workspace',
      component: Workspace,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/workspace/new',
      name: 'WorkspaceCreate',
      component: WorkspaceUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/workspace/:workspaceId/edit',
      name: 'WorkspaceEdit',
      component: WorkspaceUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/workspace/:workspaceId/view',
      name: 'WorkspaceView',
      component: WorkspaceDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/issue-list',
      name: 'IssueList',
      component: IssueList,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/issue-list/new',
      name: 'IssueListCreate',
      component: IssueListUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/issue-list/:issueListId/edit',
      name: 'IssueListEdit',
      component: IssueListUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/issue-list/:issueListId/view',
      name: 'IssueListView',
      component: IssueListDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/issue',
      name: 'Issue',
      component: Issue,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/issue/new',
      name: 'IssueCreate',
      component: IssueUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/issue/:issueId/edit',
      name: 'IssueEdit',
      component: IssueUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/issue/:issueId/view',
      name: 'IssueView',
      component: IssueDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/list-connection',
      name: 'ListConnection',
      component: ListConnection,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/list-connection/new',
      name: 'ListConnectionCreate',
      component: ListConnectionUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/list-connection/:listConnectionId/edit',
      name: 'ListConnectionEdit',
      component: ListConnectionUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/list-connection/:listConnectionId/view',
      name: 'ListConnectionView',
      component: ListConnectionDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
  ]
});
