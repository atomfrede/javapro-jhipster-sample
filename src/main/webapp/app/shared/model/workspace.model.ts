import { ITeam } from '@/shared/model/team.model';
import { IListConnection } from '@/shared/model/list-connection.model';

export interface IWorkspace {
  id?: number;
  name?: string;
  team?: ITeam;
  listConnection?: IListConnection;
}

export class Workspace implements IWorkspace {
  constructor(public id?: number, public name?: string, public team?: ITeam, public listConnection?: IListConnection) {}
}
