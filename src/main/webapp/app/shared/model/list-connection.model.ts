import { IIssueList } from '@/shared/model/issue-list.model';
import { IWorkspace } from '@/shared/model/workspace.model';

export const enum ListRole {
  INBOX = 'INBOX',
  OUTBOX = 'OUTBOX',
  INTERNAL = 'INTERNAL'
}

export interface IListConnection {
  id?: number;
  name?: string;
  type?: ListRole;
  issueLists?: IIssueList[];
  workspaces?: IWorkspace[];
}

export class ListConnection implements IListConnection {
  constructor(
    public id?: number,
    public name?: string,
    public type?: ListRole,
    public issueLists?: IIssueList[],
    public workspaces?: IWorkspace[]
  ) {}
}
