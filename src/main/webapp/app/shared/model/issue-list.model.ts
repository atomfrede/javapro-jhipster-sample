import { IIssue } from '@/shared/model/issue.model';
import { IListConnection } from '@/shared/model/list-connection.model';

export interface IIssueList {
  id?: number;
  name?: string;
  issues?: IIssue[];
  listConnection?: IListConnection;
}

export class IssueList implements IIssueList {
  constructor(public id?: number, public name?: string, public issues?: IIssue[], public listConnection?: IListConnection) {}
}
