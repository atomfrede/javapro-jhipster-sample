import { IIssueList } from '@/shared/model/issue-list.model';

export interface IIssue {
  id?: number;
  title?: string;
  issueList?: IIssueList;
}

export class Issue implements IIssue {
  constructor(public id?: number, public title?: string, public issueList?: IIssueList) {}
}
