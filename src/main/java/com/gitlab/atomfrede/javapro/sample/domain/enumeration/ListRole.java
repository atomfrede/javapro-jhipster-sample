package com.gitlab.atomfrede.javapro.sample.domain.enumeration;

/**
 * The ListRole enumeration.
 */
public enum ListRole {
    INBOX, OUTBOX, INTERNAL
}
