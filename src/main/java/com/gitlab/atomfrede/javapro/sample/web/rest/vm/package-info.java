/**
 * View Models used by Spring MVC REST controllers.
 */
package com.gitlab.atomfrede.javapro.sample.web.rest.vm;
