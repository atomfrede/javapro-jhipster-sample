package com.gitlab.atomfrede.javapro.sample.repository;

import com.gitlab.atomfrede.javapro.sample.domain.ListConnection;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ListConnection entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ListConnectionRepository extends JpaRepository<ListConnection, Long> {

}
