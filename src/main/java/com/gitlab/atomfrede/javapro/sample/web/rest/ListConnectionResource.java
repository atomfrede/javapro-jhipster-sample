package com.gitlab.atomfrede.javapro.sample.web.rest;

import com.gitlab.atomfrede.javapro.sample.domain.ListConnection;
import com.gitlab.atomfrede.javapro.sample.repository.ListConnectionRepository;
import com.gitlab.atomfrede.javapro.sample.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.gitlab.atomfrede.javapro.sample.domain.ListConnection}.
 */
@RestController
@RequestMapping("/api")
public class ListConnectionResource {

    private final Logger log = LoggerFactory.getLogger(ListConnectionResource.class);

    private static final String ENTITY_NAME = "listConnection";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ListConnectionRepository listConnectionRepository;

    public ListConnectionResource(ListConnectionRepository listConnectionRepository) {
        this.listConnectionRepository = listConnectionRepository;
    }

    /**
     * {@code POST  /list-connections} : Create a new listConnection.
     *
     * @param listConnection the listConnection to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new listConnection, or with status {@code 400 (Bad Request)} if the listConnection has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/list-connections")
    public ResponseEntity<ListConnection> createListConnection(@Valid @RequestBody ListConnection listConnection) throws URISyntaxException {
        log.debug("REST request to save ListConnection : {}", listConnection);
        if (listConnection.getId() != null) {
            throw new BadRequestAlertException("A new listConnection cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ListConnection result = listConnectionRepository.save(listConnection);
        return ResponseEntity.created(new URI("/api/list-connections/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /list-connections} : Updates an existing listConnection.
     *
     * @param listConnection the listConnection to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated listConnection,
     * or with status {@code 400 (Bad Request)} if the listConnection is not valid,
     * or with status {@code 500 (Internal Server Error)} if the listConnection couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/list-connections")
    public ResponseEntity<ListConnection> updateListConnection(@Valid @RequestBody ListConnection listConnection) throws URISyntaxException {
        log.debug("REST request to update ListConnection : {}", listConnection);
        if (listConnection.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ListConnection result = listConnectionRepository.save(listConnection);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, listConnection.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /list-connections} : get all the listConnections.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of listConnections in body.
     */
    @GetMapping("/list-connections")
    public List<ListConnection> getAllListConnections() {
        log.debug("REST request to get all ListConnections");
        return listConnectionRepository.findAll();
    }

    /**
     * {@code GET  /list-connections/:id} : get the "id" listConnection.
     *
     * @param id the id of the listConnection to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the listConnection, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/list-connections/{id}")
    public ResponseEntity<ListConnection> getListConnection(@PathVariable Long id) {
        log.debug("REST request to get ListConnection : {}", id);
        Optional<ListConnection> listConnection = listConnectionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(listConnection);
    }

    /**
     * {@code DELETE  /list-connections/:id} : delete the "id" listConnection.
     *
     * @param id the id of the listConnection to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/list-connections/{id}")
    public ResponseEntity<Void> deleteListConnection(@PathVariable Long id) {
        log.debug("REST request to delete ListConnection : {}", id);
        listConnectionRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
