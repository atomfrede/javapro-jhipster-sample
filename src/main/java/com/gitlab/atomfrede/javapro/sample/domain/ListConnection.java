package com.gitlab.atomfrede.javapro.sample.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.gitlab.atomfrede.javapro.sample.domain.enumeration.ListRole;

/**
 * A ListConnection.
 */
@Entity
@Table(name = "list_connection")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ListConnection implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private ListRole type;

    @OneToMany(mappedBy = "listConnection")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<IssueList> issueLists = new HashSet<>();

    @OneToMany(mappedBy = "listConnection")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Workspace> workspaces = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ListConnection name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ListRole getType() {
        return type;
    }

    public ListConnection type(ListRole type) {
        this.type = type;
        return this;
    }

    public void setType(ListRole type) {
        this.type = type;
    }

    public Set<IssueList> getIssueLists() {
        return issueLists;
    }

    public ListConnection issueLists(Set<IssueList> issueLists) {
        this.issueLists = issueLists;
        return this;
    }

    public ListConnection addIssueList(IssueList issueList) {
        this.issueLists.add(issueList);
        issueList.setListConnection(this);
        return this;
    }

    public ListConnection removeIssueList(IssueList issueList) {
        this.issueLists.remove(issueList);
        issueList.setListConnection(null);
        return this;
    }

    public void setIssueLists(Set<IssueList> issueLists) {
        this.issueLists = issueLists;
    }

    public Set<Workspace> getWorkspaces() {
        return workspaces;
    }

    public ListConnection workspaces(Set<Workspace> workspaces) {
        this.workspaces = workspaces;
        return this;
    }

    public ListConnection addWorkspace(Workspace workspace) {
        this.workspaces.add(workspace);
        workspace.setListConnection(this);
        return this;
    }

    public ListConnection removeWorkspace(Workspace workspace) {
        this.workspaces.remove(workspace);
        workspace.setListConnection(null);
        return this;
    }

    public void setWorkspaces(Set<Workspace> workspaces) {
        this.workspaces = workspaces;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ListConnection)) {
            return false;
        }
        return id != null && id.equals(((ListConnection) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ListConnection{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
