package com.gitlab.atomfrede.javapro.sample.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A IssueList.
 */
@Entity
@Table(name = "issue_list")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class IssueList implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "issueList")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Issue> issues = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("issueLists")
    private ListConnection listConnection;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public IssueList name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Issue> getIssues() {
        return issues;
    }

    public IssueList issues(Set<Issue> issues) {
        this.issues = issues;
        return this;
    }

    public IssueList addIssue(Issue issue) {
        this.issues.add(issue);
        issue.setIssueList(this);
        return this;
    }

    public IssueList removeIssue(Issue issue) {
        this.issues.remove(issue);
        issue.setIssueList(null);
        return this;
    }

    public void setIssues(Set<Issue> issues) {
        this.issues = issues;
    }

    public ListConnection getListConnection() {
        return listConnection;
    }

    public IssueList listConnection(ListConnection listConnection) {
        this.listConnection = listConnection;
        return this;
    }

    public void setListConnection(ListConnection listConnection) {
        this.listConnection = listConnection;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IssueList)) {
            return false;
        }
        return id != null && id.equals(((IssueList) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "IssueList{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
