package com.gitlab.atomfrede.javapro.sample.repository;

import com.gitlab.atomfrede.javapro.sample.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
