package com.gitlab.atomfrede.javapro.sample.web.rest;

import com.gitlab.atomfrede.javapro.sample.domain.IssueList;
import com.gitlab.atomfrede.javapro.sample.repository.IssueListRepository;
import com.gitlab.atomfrede.javapro.sample.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.gitlab.atomfrede.javapro.sample.domain.IssueList}.
 */
@RestController
@RequestMapping("/api")
public class IssueListResource {

    private final Logger log = LoggerFactory.getLogger(IssueListResource.class);

    private static final String ENTITY_NAME = "issueList";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IssueListRepository issueListRepository;

    public IssueListResource(IssueListRepository issueListRepository) {
        this.issueListRepository = issueListRepository;
    }

    /**
     * {@code POST  /issue-lists} : Create a new issueList.
     *
     * @param issueList the issueList to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new issueList, or with status {@code 400 (Bad Request)} if the issueList has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/issue-lists")
    public ResponseEntity<IssueList> createIssueList(@Valid @RequestBody IssueList issueList) throws URISyntaxException {
        log.debug("REST request to save IssueList : {}", issueList);
        if (issueList.getId() != null) {
            throw new BadRequestAlertException("A new issueList cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IssueList result = issueListRepository.save(issueList);
        return ResponseEntity.created(new URI("/api/issue-lists/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /issue-lists} : Updates an existing issueList.
     *
     * @param issueList the issueList to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated issueList,
     * or with status {@code 400 (Bad Request)} if the issueList is not valid,
     * or with status {@code 500 (Internal Server Error)} if the issueList couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/issue-lists")
    public ResponseEntity<IssueList> updateIssueList(@Valid @RequestBody IssueList issueList) throws URISyntaxException {
        log.debug("REST request to update IssueList : {}", issueList);
        if (issueList.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IssueList result = issueListRepository.save(issueList);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, issueList.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /issue-lists} : get all the issueLists.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of issueLists in body.
     */
    @GetMapping("/issue-lists")
    public ResponseEntity<List<IssueList>> getAllIssueLists(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of IssueLists");
        Page<IssueList> page = issueListRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /issue-lists/:id} : get the "id" issueList.
     *
     * @param id the id of the issueList to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the issueList, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/issue-lists/{id}")
    public ResponseEntity<IssueList> getIssueList(@PathVariable Long id) {
        log.debug("REST request to get IssueList : {}", id);
        Optional<IssueList> issueList = issueListRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(issueList);
    }

    /**
     * {@code DELETE  /issue-lists/:id} : delete the "id" issueList.
     *
     * @param id the id of the issueList to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/issue-lists/{id}")
    public ResponseEntity<Void> deleteIssueList(@PathVariable Long id) {
        log.debug("REST request to delete IssueList : {}", id);
        issueListRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
