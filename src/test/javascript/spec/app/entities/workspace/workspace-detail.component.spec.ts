/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import axios from 'axios';

import * as config from '@/shared/config/config';
import WorkspaceDetailComponent from '@/entities/workspace/workspace-details.vue';
import WorkspaceClass from '@/entities/workspace/workspace-details.component';
import WorkspaceService from '@/entities/workspace/workspace.service';

const localVue = createLocalVue();
const mockedAxios: any = axios;

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

jest.mock('axios', () => ({
  get: jest.fn()
}));

describe('Component Tests', () => {
  describe('Workspace Management Detail Component', () => {
    let wrapper: Wrapper<WorkspaceClass>;
    let comp: WorkspaceClass;

    beforeEach(() => {
      mockedAxios.get.mockReset();
      mockedAxios.get.mockReturnValue(Promise.resolve({}));

      wrapper = shallowMount<WorkspaceClass>(WorkspaceDetailComponent, {
        store,
        i18n,
        localVue,
        provide: { workspaceService: () => new WorkspaceService() }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', async () => {
      it('Should call load all on init', async () => {
        // GIVEN
        mockedAxios.get.mockReturnValue(Promise.resolve({ data: { id: 123 } }));

        // WHEN
        comp.retrieveWorkspace(123);
        await comp.$nextTick();

        // THEN
        expect(comp.workspace).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
