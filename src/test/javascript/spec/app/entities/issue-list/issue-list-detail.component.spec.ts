/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import axios from 'axios';

import * as config from '@/shared/config/config';
import IssueListDetailComponent from '@/entities/issue-list/issue-list-details.vue';
import IssueListClass from '@/entities/issue-list/issue-list-details.component';
import IssueListService from '@/entities/issue-list/issue-list.service';

const localVue = createLocalVue();
const mockedAxios: any = axios;

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

jest.mock('axios', () => ({
  get: jest.fn()
}));

describe('Component Tests', () => {
  describe('IssueList Management Detail Component', () => {
    let wrapper: Wrapper<IssueListClass>;
    let comp: IssueListClass;

    beforeEach(() => {
      mockedAxios.get.mockReset();
      mockedAxios.get.mockReturnValue(Promise.resolve({}));

      wrapper = shallowMount<IssueListClass>(IssueListDetailComponent, {
        store,
        i18n,
        localVue,
        provide: { issueListService: () => new IssueListService() }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', async () => {
      it('Should call load all on init', async () => {
        // GIVEN
        mockedAxios.get.mockReturnValue(Promise.resolve({ data: { id: 123 } }));

        // WHEN
        comp.retrieveIssueList(123);
        await comp.$nextTick();

        // THEN
        expect(comp.issueList).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
