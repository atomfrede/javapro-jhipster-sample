/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import axios from 'axios';

import * as config from '@/shared/config/config';
import ListConnectionDetailComponent from '@/entities/list-connection/list-connection-details.vue';
import ListConnectionClass from '@/entities/list-connection/list-connection-details.component';
import ListConnectionService from '@/entities/list-connection/list-connection.service';

const localVue = createLocalVue();
const mockedAxios: any = axios;

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

jest.mock('axios', () => ({
  get: jest.fn()
}));

describe('Component Tests', () => {
  describe('ListConnection Management Detail Component', () => {
    let wrapper: Wrapper<ListConnectionClass>;
    let comp: ListConnectionClass;

    beforeEach(() => {
      mockedAxios.get.mockReset();
      mockedAxios.get.mockReturnValue(Promise.resolve({}));

      wrapper = shallowMount<ListConnectionClass>(ListConnectionDetailComponent, {
        store,
        i18n,
        localVue,
        provide: { listConnectionService: () => new ListConnectionService() }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', async () => {
      it('Should call load all on init', async () => {
        // GIVEN
        mockedAxios.get.mockReturnValue(Promise.resolve({ data: { id: 123 } }));

        // WHEN
        comp.retrieveListConnection(123);
        await comp.$nextTick();

        // THEN
        expect(comp.listConnection).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
