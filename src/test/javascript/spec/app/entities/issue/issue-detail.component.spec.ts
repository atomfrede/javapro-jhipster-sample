/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import axios from 'axios';

import * as config from '@/shared/config/config';
import IssueDetailComponent from '@/entities/issue/issue-details.vue';
import IssueClass from '@/entities/issue/issue-details.component';
import IssueService from '@/entities/issue/issue.service';

const localVue = createLocalVue();
const mockedAxios: any = axios;

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

jest.mock('axios', () => ({
  get: jest.fn()
}));

describe('Component Tests', () => {
  describe('Issue Management Detail Component', () => {
    let wrapper: Wrapper<IssueClass>;
    let comp: IssueClass;

    beforeEach(() => {
      mockedAxios.get.mockReset();
      mockedAxios.get.mockReturnValue(Promise.resolve({}));

      wrapper = shallowMount<IssueClass>(IssueDetailComponent, {
        store,
        i18n,
        localVue,
        provide: { issueService: () => new IssueService() }
      });
      comp = wrapper.vm;
    });

    describe('OnInit', async () => {
      it('Should call load all on init', async () => {
        // GIVEN
        mockedAxios.get.mockReturnValue(Promise.resolve({ data: { id: 123 } }));

        // WHEN
        comp.retrieveIssue(123);
        await comp.$nextTick();

        // THEN
        expect(comp.issue).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
