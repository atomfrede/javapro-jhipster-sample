/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import axios from 'axios';

import * as config from '@/shared/config/config';
import TeamDetailComponent from '@/entities/team/team-details.vue';
import TeamClass from '@/entities/team/team-details.component';
import TeamService from '@/entities/team/team.service';

const localVue = createLocalVue();
const mockedAxios: any = axios;

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

jest.mock('axios', () => ({
  get: jest.fn()
}));

describe('Component Tests', () => {
  describe('Team Management Detail Component', () => {
    let wrapper: Wrapper<TeamClass>;
    let comp: TeamClass;

    beforeEach(() => {
      mockedAxios.get.mockReset();
      mockedAxios.get.mockReturnValue(Promise.resolve({}));

      wrapper = shallowMount<TeamClass>(TeamDetailComponent, { store, i18n, localVue, provide: { teamService: () => new TeamService() } });
      comp = wrapper.vm;
    });

    describe('OnInit', async () => {
      it('Should call load all on init', async () => {
        // GIVEN
        mockedAxios.get.mockReturnValue(Promise.resolve({ data: { id: 123 } }));

        // WHEN
        comp.retrieveTeam(123);
        await comp.$nextTick();

        // THEN
        expect(comp.team).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
